-- Active: 1709546041278@@127.0.0.1@3306@blog

DROP TABLE IF EXISTS outfit;
DROP TABLE IF EXISTS collection;

CREATE TABLE collection (
  Id INT PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  image varchar(255) NOT NULL,
  description varchar(1000) NOT NULL
);


CREATE TABLE outfit (
  Id INT PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  image varchar(255) NOT NULL,
  description varchar(1000) NOT NULL,
  collection_id INT NOT NULL,
  FOREIGN KEY (collection_id) REFERENCES collection (Id) ON DELETE CASCADE
);



INSERT INTO collection (name, image, description) VALUES
('La collection soirée','https://img.freepik.com/vecteurs-libre/createur-mode-dessine-main-atelier_52683-56552.jpg?w=1380&t=st=1713531801~exp=1713532401~hmac=897d3c08cc61e125da59cabf0f1787d2686aa8e65db6ec14d71966ed4606cc27','Une collection de robes de soirée captiverait par son raffinement intemporel et son allure élégante. Elle serait soigneusement conçue pour évoquer un sentiment de glamour et de sophistication, avec une attention particulière portée aux détails et aux finitions. Chaque robe serait méticuleusement fabriquée à partir de tissus somptueux tels que la soie, le satin, ou la dentelle, offrant une sensation luxueuse au toucher.'),
('La collection soirée','https://img.freepik.com/vecteurs-libre/createur-mode-dessine-main-atelier_52683-56552.jpg?w=1380&t=st=1713531801~exp=1713532401~hmac=897d3c08cc61e125da59cabf0f1787d2686aa8e65db6ec14d71966ed4606cc27','Une collection de robes de soirée captiverait par son raffinement intemporel et son allure élégante. Elle serait soigneusement conçue pour évoquer un sentiment de glamour et de sophistication, avec une attention particulière portée aux détails et aux finitions. Chaque robe serait méticuleusement fabriquée à partir de tissus somptueux tels que la soie, le satin, ou la dentelle, offrant une sensation luxueuse au toucher.'),
('La collection soirée','https://img.freepik.com/vecteurs-libre/createur-mode-dessine-main-atelier_52683-56552.jpg?w=1380&t=st=1713531801~exp=1713532401~hmac=897d3c08cc61e125da59cabf0f1787d2686aa8e65db6ec14d71966ed4606cc27','Une collection de robes de soirée captiverait par son raffinement intemporel et son allure élégante. Elle serait soigneusement conçue pour évoquer un sentiment de glamour et de sophistication, avec une attention particulière portée aux détails et aux finitions. Chaque robe serait méticuleusement fabriquée à partir de tissus somptueux tels que la soie, le satin, ou la dentelle, offrant une sensation luxueuse au toucher.'),
('La collection soirée','https://img.freepik.com/vecteurs-libre/createur-mode-dessine-main-atelier_52683-56552.jpg?w=1380&t=st=1713531801~exp=1713532401~hmac=897d3c08cc61e125da59cabf0f1787d2686aa8e65db6ec14d71966ed4606cc27','Une collection de robes de soirée captiverait par son raffinement intemporel et son allure élégante. Elle serait soigneusement conçue pour évoquer un sentiment de glamour et de sophistication, avec une attention particulière portée aux détails et aux finitions. Chaque robe serait méticuleusement fabriquée à partir de tissus somptueux tels que la soie, le satin, ou la dentelle, offrant une sensation luxueuse au toucher.'),
('La collection soirée','https://img.freepik.com/vecteurs-libre/createur-mode-dessine-main-atelier_52683-56552.jpg?w=1380&t=st=1713531801~exp=1713532401~hmac=897d3c08cc61e125da59cabf0f1787d2686aa8e65db6ec14d71966ed4606cc27','Une collection de robes de soirée captiverait par son raffinement intemporel et son allure élégante. Elle serait soigneusement conçue pour évoquer un sentiment de glamour et de sophistication, avec une attention particulière portée aux détails et aux finitions. Chaque robe serait méticuleusement fabriquée à partir de tissus somptueux tels que la soie, le satin, ou la dentelle, offrant une sensation luxueuse au toucher.'),
('La collection mariée','https://img.freepik.com/vecteurs-libre/illustration-caractere-du-mode-vie-femme_53876-37304.jpg?w=1800&t=st=1713531859~exp=1713532459~hmac=54473bc3d5a341a1074a07c478e6263032c6b2844c3b52e3d58adc557872538d','La collection de robes de mariée et de costumes pour hommes incarnerait l\'élégance intemporelle pour un mariage mémorable. Les robes offriraient des silhouettes classiques avec des détails délicats, tandis que les costumes pour hommes se distingueraient par une coupe impeccable et des finitions sophistiquées, disponibles dans une palette de couleurs élégantes.');


INSERT INTO outfit (name, image,description,collection_id) VALUES 
('Lune de Miel','https://fr.freepik.com/vecteurs-libre/partie-nuit-robes-mignonnes_901169.htm#fromView=search&page=1&position=28&uuid=55af23c6-01c5-4516-ad2a-e4d0fee16430','Une robe de soirée éblouissante avec un corsage en dentelle délicate et une jupe fluide en mousseline. L\'encolure illusion et les détails perlés ajoutent une touche de glamour subtile, parfaitement adaptée pour une soirée élégante sous les étoiles.',1),
('Etoile Filante','https://fr.freepik.com/vecteurs-libre/illustration-personnage-conte-fees-cendrillon_7080171.htm#fromView=search&page=3&position=41&uuid=55af23c6-01c5-4516-ad2a-e4d0fee16430','Une robe de soirée sirène en satin luxueux, mettant en valeur la silhouette avec une coupe ajustée jusqu\'aux hanches et une jupe évasée spectaculaire. Les ornements en strass scintillants sur les bretelles spaghetti ajoutent une touche de brillance à cette création sophistiquée, idéale pour briller sur le tapis rouge.',1),
('Nuit Enchantée','https://fr.freepik.com/vecteurs-libre/illustration-personnage-conte-fees-cendrillon_7080164.htm#from_view=detail_alsolike','Une robe de soirée romantique en tulle doux, avec une encolure illusion ornée de broderies délicates et de perles chatoyantes. La jupe évasée et les détails en dentelle florale créent une allure magique, parfaite pour une nuit de célébration et de féérie.',1),
('Lumière Céleste','https://fr.freepik.com/vecteurs-libre/belle-robe-mariee_850888.htm#fromView=search&page=2&position=3&uuid=3250bc60-5716-4c62-ba64-cbf6e7ec9725','Une robe de mariée éthérée en mousseline légère, avec un corsage délicatement drapé et une jupe fluide. L\'encolure illusion ornée de motifs en dentelle ajoute une touche de romantisme, tandis que le dos ouvert apporte une dose de modernité à cette création intemporelle.',2),
('Élégance Classique','https://fr.freepik.com/vecteurs-libre/illustration-garde-du-corps-dessinee-main_40126631.htm#fromView=search&page=1&position=20&uuid=ef5bef10-ea86-4f3d-90ed-3e667f03c5ea','Un costume pour homme classique en laine vierge, avec une veste ajustée à double boutonnage et des pantalons élégamment coupés. Les revers crantés et les boutons contrastants ajoutent une touche de sophistication, parfait pour un mariage traditionnel ou contemporain.',2),
('Raffinement Moderne','https://fr.freepik.com/vecteurs-libre/illustration-homme-mode-dessine-main_4970152.htm#fromView=search&page=1&position=1&uuid=4502e53c-8178-4f00-a030-5c7e75921bf4','Un costume pour homme contemporain en tweed texturé, avec une veste slim fit et des pantalons fuselés. Les détails subtils comme les poches passepoilées et les boutons ton sur ton incarnent l\'élégance discrète, idéale pour un mariage urbain chic ou une cérémonie en plein air.',2);

