package co.simplon.promo27.blogcouture.blogcouture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import co.simplon.promo27.blogcouture.blogcouture.entities.Outfit;
import co.simplon.promo27.blogcouture.blogcouture.repository.OutfitRepository;


@SpringBootTest
@AutoConfigureMockMvc
@Sql("/database.sql")
public class OutfitTest {
    
        @Autowired
        MockMvc mvc;
        @Autowired
        OutfitRepository repo;
    
    @Test 
    void findAll (){
        List<Outfit> result = repo.findAll();
        
        assertEquals(1, result.get(0).getId());
        assertEquals("Lune de Miel", result.get(0).getName());
        assertEquals("https://fr.freepik.com/vecteurs-libre/partie-nuit-robes-mignonnes_901169.htm#fromView=search&page=1&position=28&uuid=55af23c6-01c5-4516-ad2a-e4d0fee16430", result.get(0).getImage());
        assertEquals("Une robe de soirée éblouissante avec un corsage en dentelle délicate et une jupe fluide en mousseline. L\'encolure illusion et les détails perlés ajoutent une touche de glamour subtile, parfaitement adaptée pour une soirée élégante sous les étoiles.", result.get(0).getDescription());
        assertEquals(1, result.get(0).getCollection_id());
    }
    
    @Test 
    void findById (){
    Outfit result = repo.findById(1);

    assertEquals(1, result.getId());
    assertEquals("Lune de Miel", result.getName());
    assertEquals("https://fr.freepik.com/vecteurs-libre/partie-nuit-robes-mignonnes_901169.htm#fromView=search&page=1&position=28&uuid=55af23c6-01c5-4516-ad2a-e4d0fee16430", result.getImage());
    assertEquals("Une robe de soirée éblouissante avec un corsage en dentelle délicate et une jupe fluide en mousseline. L\'encolure illusion et les détails perlés ajoutent une touche de glamour subtile, parfaitement adaptée pour une soirée élégante sous les étoiles.", result.getDescription());
    assertEquals(1, result.getCollection_id());
}
    
    @Test
    void findByIdNoResult() {
        Outfit result = repo.findById(1000);
    
        assertNull(result);
    }
    
    @Test
    void persist() {
        Outfit outfit = new Outfit ("test name", "test image","test description", 1);
        assertTrue(repo.persist(outfit));
        assertEquals(7, outfit.getId());
    }
    
    @Test
    void update() {
        Outfit outfit = new Outfit (1,"test name", "test image","test description",1);
        assertTrue(repo.updateOutfit(outfit));
        
        Outfit updated = repo.findById(1);
        assertEquals("test name", updated.getName());
        assertEquals("test image", updated.getImage());
        assertEquals("test description", updated.getDescription());
        assertEquals(1, updated.getCollection_id());

    }
    
    @Test
    void delete(){
        boolean result = repo.deleteOutfit(1);
        assertTrue(result);
    } 
    
    }
    

