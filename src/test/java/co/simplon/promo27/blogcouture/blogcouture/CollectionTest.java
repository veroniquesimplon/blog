package co.simplon.promo27.blogcouture.blogcouture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import co.simplon.promo27.blogcouture.blogcouture.entities.Collection;
import co.simplon.promo27.blogcouture.blogcouture.repository.CollectionRepository;


@SpringBootTest
@AutoConfigureMockMvc
@Sql("/database.sql")
public class CollectionTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    CollectionRepository repo;

@Test 
void findAll (){
    List<Collection> result = repo.findAll();
    
    assertEquals(1, result.get(0).getId());
    assertEquals("La collection soirée", result.get(0).getName());
    assertEquals("https://fr.freepik.com/vecteurs-libre/createur-mode-dessine-main-atelier_12553826.htm#fromView=search&page=1&position=4&uuid=848148b3-3e44-4ca0-80f3-26bc4501d9eb", result.get(0).getImage());
    assertEquals("Une collection de robes de soirée captiverait par son raffinement intemporel et son allure élégante. Elle serait soigneusement conçue pour évoquer un sentiment de glamour et de sophistication, avec une attention particulière portée aux détails et aux finitions. Chaque robe serait méticuleusement fabriquée à partir de tissus somptueux tels que la soie, le satin, ou la dentelle, offrant une sensation luxueuse au toucher.", result.get(0).getDescription());
}

@Test 
void findById (){
Collection result = repo.findById(1);

assertEquals(1, result.getId());
assertEquals("La collection soirée", result.getName());
assertEquals("https://fr.freepik.com/vecteurs-libre/createur-mode-dessine-main-atelier_12553826.htm#fromView=search&page=1&position=4&uuid=848148b3-3e44-4ca0-80f3-26bc4501d9eb", result.getImage());
assertEquals("Une collection de robes de soirée captiverait par son raffinement intemporel et son allure élégante. Elle serait soigneusement conçue pour évoquer un sentiment de glamour et de sophistication, avec une attention particulière portée aux détails et aux finitions. Chaque robe serait méticuleusement fabriquée à partir de tissus somptueux tels que la soie, le satin, ou la dentelle, offrant une sensation luxueuse au toucher.", result.getDescription());
}

@Test
void findByIdNoResult() {
    Collection result = repo.findById(1000);

    assertNull(result);
}

@Test
void persist() {
    Collection collection = new Collection ("test name", "test image","test description");
    assertTrue(repo.persist(collection));
    assertEquals(3, collection.getId());
}

@Test
void update() {
    Collection collection = new Collection (1,"test name", "test image","test description");
    assertTrue(repo.updateCollection(collection));
    
    Collection updated = repo.findById(1);
    assertEquals("test name", updated.getName());
    assertEquals("test image", updated.getImage());
    assertEquals("test description", updated.getDescription());

}

@Test
void delete(){
    boolean result = repo.deleteCollection(1);
    assertTrue(result);
}



}
