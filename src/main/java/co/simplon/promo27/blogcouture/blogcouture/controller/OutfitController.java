package co.simplon.promo27.blogcouture.blogcouture.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo27.blogcouture.blogcouture.entities.Outfit;
import co.simplon.promo27.blogcouture.blogcouture.repository.OutfitRepository;
import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/outfit")
public class OutfitController {
    @Autowired
    private OutfitRepository outfitRepo;

@GetMapping("/{id}")
public Outfit one(@PathVariable int id) {
    Outfit collection = outfitRepo.findById(id);
    if (collection == null) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return collection;
}
@PostMapping
@ResponseStatus(HttpStatus.CREATED)
public Outfit add( @Valid @RequestBody Outfit outfit) {
    outfitRepo.persist(outfit);
    return outfit;
}
@DeleteMapping("/{id}")
@ResponseStatus(HttpStatus.NO_CONTENT)
public void remove(@PathVariable int id) {
    one(id);
    outfitRepo.deleteOutfit(id);
}
@PutMapping("/{id}")
public Outfit replace(@PathVariable int id, @Valid @RequestBody Outfit outfit) {
    one(id); 
    outfit.setId(id);
    outfitRepo.updateOutfit(outfit);
    return outfit;
}
}
