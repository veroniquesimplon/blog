package co.simplon.promo27.blogcouture.blogcouture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogcoutureApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogcoutureApplication.class, args);
	}

}
