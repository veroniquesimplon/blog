package co.simplon.promo27.blogcouture.blogcouture.entities;

public class Outfit {
    private Integer id;
    private String name;
    private String image;
    private String description;
    private Integer collection_id;
   
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCollection_id() {
        return collection_id;
    }

    public void setCollection_id(Integer collection_id) {
        this.collection_id = collection_id;
    }

    public Outfit() {
    }

    public Outfit(String name, String image, String description, Integer collection_id) {
        this.name = name;
        this.image = image;
        this.description = description;
        this.collection_id = collection_id;
    }

    public Outfit(Integer id, String name, String image, String description, Integer collection_id) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.description = description;
        this.collection_id = collection_id;
    }

}
