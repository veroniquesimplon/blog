package co.simplon.promo27.blogcouture.blogcouture.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.blogcouture.blogcouture.entities.Collection;

@Repository
public class CollectionRepository {
    
    @Autowired
    private DataSource dataSource;

    public List<Collection> findAll() {
        List<Collection> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM collection");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Collection collection = new Collection(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("image"),
                        result.getString("description"));
                list.add(collection);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository");
        }
        return list;
    }

    public Collection findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM collection WHERE id =?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Collection(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("image"),
                        result.getString("description"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository");
        }
        return null;
    }

    public boolean persist(Collection collection) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO collection (name, image,description)VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, collection.getName());
            stmt.setString(2, collection.getImage());
            stmt.setString(3, collection.getDescription());
            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                collection.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error in repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateCollection(Collection collection) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("Update collection SET name=?, image=?,description=? WHERE id=?");
            stmt.setString(1, collection.getName());
            stmt.setString(2, collection.getImage());
            stmt.setString(3, collection.getDescription());
            stmt.setInt(4,collection.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }
            } catch (SQLException e) {
            System.out.println("Error in repository");
            e.printStackTrace();
        }
        return false;
    
    }

    public boolean deleteCollection(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM outfit WHERE id=?");

            
            stmt.setInt(1, id);

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }



}
