package co.simplon.promo27.blogcouture.blogcouture.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.blogcouture.blogcouture.entities.Outfit;

@Repository
public class OutfitRepository {

    @Autowired
    private DataSource dataSource;

    public List<Outfit> findAll() {
        List<Outfit> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM outfit");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Outfit outfit = new Outfit(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("image"),
                        result.getString("description"),
                        result.getInt("collection_id"));
                list.add(outfit);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository");
        }
        return list;
    }

    public Outfit findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM outfit WHERE id =?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Outfit(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("image"),
                        result.getString("description"),
                        result.getInt("collection_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository");
        }
        return null;
    }

    public boolean persist(Outfit outfit) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO outfit (name, image,description,collection_id)VALUES (?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, outfit.getName());
            stmt.setString(2, outfit.getImage());
            stmt.setString(3, outfit.getDescription());
            stmt.setInt(4, outfit.getCollection_id());
            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                outfit.setId(keys.getInt(1));
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error in repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateOutfit(Outfit outfit) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("Update outfit SET name=?, image=?,description=?, collection_id=? WHERE id=?");
                    stmt.setString(1, outfit.getName());
                    stmt.setString(2, outfit.getImage());
                    stmt.setString(3, outfit.getDescription());
                    stmt.setInt(4, outfit.getCollection_id());
                    stmt.setInt(5, outfit.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error in repository");
            e.printStackTrace();
        }
        return false;

    }

    public boolean deleteOutfit(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM outfit WHERE id=?");

            stmt.setInt(1, id);

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

}
