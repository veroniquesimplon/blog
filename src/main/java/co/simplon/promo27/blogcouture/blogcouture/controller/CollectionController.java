package co.simplon.promo27.blogcouture.blogcouture.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo27.blogcouture.blogcouture.entities.Collection;
import co.simplon.promo27.blogcouture.blogcouture.repository.CollectionRepository;
import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/collection")
public class CollectionController {

@Autowired
private CollectionRepository collectionRepo;

@GetMapping
public List<Collection> all (){
    return collectionRepo.findAll();
}

@GetMapping("/{id}")
public Collection one (@PathVariable int id){
    Collection collection = collectionRepo.findById(id);
    if (collection == null) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return collection;
}

@PostMapping
@ResponseStatus(HttpStatus.CREATED)
public Collection add( @Valid @RequestBody Collection collection) {
    collectionRepo.persist(collection);
    return collection;
}

@PutMapping("/{id}")
public Collection replace(@PathVariable int id, @Valid @RequestBody Collection collection) {
    one(id); 
    collection.setId(id);
    collectionRepo.updateCollection(collection);
    return collection;
}
@DeleteMapping("/{id}")
@ResponseStatus(HttpStatus.NO_CONTENT)
public void remove(@PathVariable int id) {
    one(id);
    collectionRepo.deleteCollection(id);
}
}


